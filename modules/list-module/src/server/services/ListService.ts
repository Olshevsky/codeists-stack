import { inject, injectable } from "inversify";

import { Types } from "../constants";
import { IListService, IListRepository } from "../interfaces";

@injectable()
export class ListService implements IListService {
    constructor(
        @inject(Types.ListRepository) private repository: IListRepository
    ) {  }
}
