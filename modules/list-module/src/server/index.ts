import * as _ from 'lodash';
import { Router } from 'express';
import { interfaces } from 'inversify';
import { feature } from '@charlotte/core';
import { makeExecutableSchema } from 'graphql-tools';
import { IGraphqlFeature } from '@charlotte/core-graphql';

import { Types } from './constants';
import { container } from './container';

const services = (container: interfaces.Container) => ({
    lists: container.get(Types.ListService),
});

const router = (settings, context) => {
    const router = Router();
    return router;
};

export default feature<IGraphqlFeature>({
    router,
    services,
    container,
} as any);
