import { Container } from 'inversify';

import { Types } from '../constants';
import { IListRepository } from '../interfaces';
import { ListRepository } from './repositories';

export const store = new Container();
store.bind<IListRepository>(Types.ListRepository).to(ListRepository);
