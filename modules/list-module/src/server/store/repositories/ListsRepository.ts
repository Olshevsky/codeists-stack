import { Model, Connection } from 'mongoose'; 
import { inject, injectable } from 'inversify'; 

import { ListModelFunc } from '../models'; 
import { IListRepository } from "../../interfaces";

@injectable()
export class ListRepository implements IListRepository {
    private model: Model<any>;

    constructor(
        @inject('db.connection') connection: Connection,
    ) {
        this.model = ListModelFunc(connection);
    }
}
