import { Connection, model, Model, Schema } from 'mongoose';
import * as Timestamp from 'mongoose-timestamp';

const ListSchema = new Schema({

});

ListSchema.plugin(Timestamp);

export const ListModelFunc = (connection: Connection): Model<any> => 
    connection.model('List', ListSchema);
