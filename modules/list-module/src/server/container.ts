import { Container } from 'inversify';

import { Types } from './constants';
import { ListService } from './services';
import { store } from './store/container';
import { IListService } from './interfaces';

const services = new Container();
services.bind<IListService>(Types.ListService).to(ListService);

export const container = Container.merge(services, store);
