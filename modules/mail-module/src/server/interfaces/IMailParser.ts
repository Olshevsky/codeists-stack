export interface IMailParser {
    parse(mail: any): Promise<boolean>;
}
