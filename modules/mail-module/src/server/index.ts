import * as _ from 'lodash';
import { Router } from 'express';
import { interfaces } from 'inversify';
import { feature } from '@charlotte/core';
import { makeExecutableSchema } from 'graphql-tools';
import { IGraphqlFeature } from '@charlotte/core-graphql';

import { Types } from './constants';
import { container } from './container';

const services = (container: interfaces.Container) => ({
    mailParser: container.get(Types.MailParser),
});

const router = (settings: any, context) => {
    const router = Router();

    router.get('/.hooks/sendgrid/parse', (req, res, next) => {
        context.mailParser.parse(req.body)
            .then(data => console.log('Mail parsed!'))
            .catch((err) => console.log('Parse Error: ', err));

        res.json({ ok: true });
    });

    return router;
}

export default feature<IGraphqlFeature>({
    router,
    services,
    container,
} as any);
