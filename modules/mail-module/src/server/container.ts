import { Container } from "inversify";

import { Types } from "./constants";
import { SendGridParser } from "./services";

export const container = new Container();

container.bind<any>(Types.MailParser).to(SendGridParser);
