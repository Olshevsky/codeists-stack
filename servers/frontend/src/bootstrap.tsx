declare const location: any;
declare const document: any;

import * as _ from 'lodash';
import { provide } from 'ioc';
import * as React from 'react';
import * as qs from 'query-string';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ApolloProvider } from 'react-apollo';
import UniversalRouter from 'universal-router';
import createBrowserHistory from 'history/createBrowserHistory';
import { IReactFeature, createStore, createApollo } from '@charlotte/core-client';

import { Application } from './Application';

const history = createBrowserHistory();
let current = history.location;

const normalize = (list: any[]) => (list || []).sort((s1, s2) => parseInt(s1.index || s1.root ? "9999" : "0") - parseInt(s2.index || s2.root ? "9999" : "0"));

export const tree = (list) => {
    const nested = (root, list) => {
        if(!root.id) return root;
        return _.assign({}, root, {
            children: (root.children || []).concat(list
                .filter(item => item.top === root.id)
                .map(child => nested(child, list)))
        });
    };

    return list.filter(item => !item.top).map(root => nested(root, list));
};

export async function start(module: IReactFeature) {
    // Build route tree, routes normalization
    const ordered = normalize(module.routes);
    const children = tree(_.cloneDeep(ordered));

    // Config application elements
    const store = createStore(module);

    const list: any[] = await Promise.all(_.map(module.context, context => context(module.settings)));
    const context = (_ as any).merge(...list);
    
    const apollo = await createApollo(module, context);

    const router = { children };
    const getRouteParams = () => ({
        context,
        history,
        settings: module.settings,
        pathname: current.pathname,
        query: qs.parse(current.search),
    });

    // Create router after service creation
    const handler = new UniversalRouter(router, {
        resolveRoute: async (context, params) => {
            if (typeof context.route.load === 'function') {
                return context.route
                    .load()
                    .then(action => action.default(context, params));
            }

            if (typeof context.route.action === 'function') {
                return context.route.action(context, params);
            }

            return undefined;
        },
    });

    async function resolve(location: any) {
        current = location;

        console.log('Resolving: ', context);

        // Resolve route for path
        const route = await handler.resolve({
            context,
            history,
            settings: module.settings,
            pathname: current.pathname,
            query: qs.parse(location.search),
        });

        // Stop re-render
        if (current.key !== location.key) {
            return;
        }

        // Handle application redirects
        if (route.redirect) {
            history.replace(route.redirect);
            return;
        }

        return route;
    }

    async function listener(location, action?) {
        // Fetch root component
        const isInitialRender = !action;

        // Resolve route
        const route = await resolve(location) as any;
        const IOCContainer = provide(module)(Application);

        const RouteLayout = route.layout 
            ? route.layout 
            : ({ children }) => <React.Fragment>{children}</React.Fragment>;

        const CombinedApplication = props => (
            <ApolloProvider client={apollo}>
                <Provider store={store}>
                    <IOCContainer>
                        <RouteLayout {...getRouteParams()}>
                            {props.children}
                        </RouteLayout>
                    </IOCContainer>
                </Provider>
            </ApolloProvider>
        );

        return render((
            <CombinedApplication>
                {route.component}
            </CombinedApplication>
        ), isInitialRender);
    };

    // Subscribe for history updates
    history.listen(listener);

    // Initial renderer
    return await listener(location, false);
}

export function render(elements, isInitialRender = true) {
    const render = isInitialRender ? ReactDOM.render : ReactDOM.hydrate;

    return render(
        elements,
        document.getElementById("root"),
    );
}
