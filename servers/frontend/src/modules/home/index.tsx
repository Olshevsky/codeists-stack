import React from "react";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { feature } from "@charlotte/core";
import { IReactFeature } from "@charlotte/core-client";

const HEALTH_QUERY = gql`
    query health {
        health
    }
`;

export default feature<IReactFeature>({
    routes: [
        { path: '/', action: () => ({ component: (
            <div>
                <h1>Charlotte</h1>
                <Query query={HEALTH_QUERY}>
                    {({ error, data }) => (<pre>{JSON.stringify(data)}</pre>)}
                </Query>
            </div>
        ) }) },
    ],
});