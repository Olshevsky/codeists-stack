import * as React from "react";
import { feature } from "@charlotte/core";
import { Container } from "inversify";
import { IReactFeature, ROUTE_INDEX } from "@charlotte/core-client";

export default feature<IReactFeature>({ 
    container: new Container, 
    settings: {
        AUTH0_DOMAIN: 'adlab.auth0.com',
        AUTH0_CLIENT_ID: 'bz0pXs9vd0O9u19Y9m31NqJ6w8GeOjFn',
        AUTH0_REDIRECT_URI: 'http://localhost:1234/callback',

        APOLLO_URI: 'http://localhost:9090/graphql',
    },
    reducers: {
        "@module-stack/__init__": (state = true) => state,
    },
    routes: [
        { path: '(.*)', index: ROUTE_INDEX.SYSTEM_ROUTE, action: () => ({ component: <div>Not found!</div> }) },
    ],
});
