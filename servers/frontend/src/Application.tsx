import * as React from "react";

export class Application extends React.Component<any, any> {
    public render() {
        return (
            <div>{this.props.children}</div>
        );
    }
}
