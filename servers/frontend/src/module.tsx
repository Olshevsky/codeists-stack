import * as React from 'react';
import { Container } from 'inversify';
import { feature, composed } from '@charlotte/core';
import AuthAPIModule from '@charlotte/auth-api/lib/client';
import { SchemaReactFeature } from '@charlotte/core-client';

import { HomeModule, ConfigModule } from './modules';

// Build application
const application = feature(HomeModule, ConfigModule, (AuthAPIModule as any).default, {
    routes: [
        { path: '/login', load: () => import('@charlotte/auth-api/lib/client/routes/login') },
        { path: '/logout', load: () => import('@charlotte/auth-api/lib/client/routes/logout') },
        { path: '/profile', load: () => import('@charlotte/auth-api/lib/client/routes/profile') },
        { path: '/callback', load: () => import('@charlotte/auth-api/lib/client/routes/callback') },
    ],
});

// Composed module for configuration
export const module = composed(application, SchemaReactFeature);
