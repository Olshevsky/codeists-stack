import { Container } from "inversify";
import { feature } from "@charlotte/core";
import { IGraphqlFeature } from "@charlotte/core-graphql";

import { env } from '../env';

export const ConfigModule = feature<IGraphqlFeature>({ 
    settings: env,
    container: new Container(),
    options: {
        port: 9090,
        uploads: undefined,
        cacheControl: true,
        endpoint: '/graphql',
        subscriptions: '/graphql',
        playground: '/playground',
    } as any,
})
