import { feature } from "@charlotte/core";
import { IGraphqlFeature } from "@charlotte/core-graphql";

import { router } from './router';
import { schema } from './schema';

export const SystemModule = feature<IGraphqlFeature>({ 
    router,
    schema: schema as any,
});
