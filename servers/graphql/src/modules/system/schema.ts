import { makeExecutableSchema } from "graphql-tools";

const typeDefs = `
    type Query {
        health: String @isAuthenticated
    }
`;

const resolvers = ctx => ({
    Query: {
        health: (root, args, context) => `ok`,
    },
});

export const schema = (ctx) => makeExecutableSchema({ 
    typeDefs, 
    resolvers: resolvers(ctx), 
});
