///<reference types="webpack-env" />

import 'reflect-metadata';
import 'isomorphic-fetch';

import { application } from './module';
import { start } from './bootstrap';

start(application)
    .then(() => console.log('> Graphql server running...'))
    .catch((err) => console.error('Cannot start graphql server: ', err));

if (module.hot) {
    module.hot.status(event => {
        if (event === 'abort' || event === 'fail') {
            // Signal webpack.run.js to do full-reload of the back-end
            process.exit(250);
        }
        if (event === 'idle') {
            process.exit(250);
        }
    });

    module.hot.accept();
}
