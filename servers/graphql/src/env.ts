import { resolve } from 'path';
import * as dotenv from 'dotenv';
import * as envalid from 'envalid';

// Get file name
const file = (nodeEnv = 'development') => `${nodeEnv}.env`
const path = resolve(__dirname, process.env.ENV_PATH, file(process.env.NODE_ENV))

// Load config 
const config = dotenv.load({ path });
const { str } = envalid;

export const env = envalid.cleanEnv(config.parsed, {
    JWT_SECRET: str(),
    TMP_DIR: str({ default: '/tmp/imgs' }),

    AUTH0_DOMAIN: str(),
    AUTH0_SECRET: str(),
    AUTH0_ISSUER: str(),
    AUTH0_AUDIENCE: str(),
    AUTH0_CLIENT_ID: str(),
    AUTH0_API_AUDIENCE: str(),
    AUTH0_REDIRECT_URI: str(),

    MONGODB_URI: str({ default: 'mongodb://localhost:27017/charlotte-stack' })
});
