/// <reference types="webpack-env" />

import * as _ from 'lodash';
import * as cors from 'cors';
import { Container } from 'inversify';
import { mergeSchemas } from 'graphql-tools';
import { connect, createConnection } from 'mongoose';
import { IGraphqlFeature, TYPES } from '@charlotte/core-graphql';
import { GraphQLServer, Options as  ServerOptions } from 'graphql-yoga';

export async function start(feature: IGraphqlFeature) {
    const connection = createConnection(feature.settings.MONGODB_URI, { useNewUrlParser: true });

    const list = {
        request: (request) => _.map(feature.request, record => record(request)),
        mocks: (context) => Promise.all(_.map(feature.mocks, record => record(context))),
        schema: (context) => Promise.all(_.map(feature.schema as any, schema => schema(context))),
        context: await Promise.all(_.map(feature.context, context => context(feature.settings))),
        middlewares: (context) => Promise.all(_.map(feature.middlewares as any, record => record(context))),
        services: (container) => Promise.all(_.map(feature.services, service => service(container))),
        directives: (context) => Promise.all(_.map(feature.schemaDirectives, record => record(context))),
        router: (settings, context) => Promise.all(_.map(feature.router as any, record => record(settings, context))),
    };

    const container = feature.container || new Container();

    // Set Settings!
    container.bind('settings').toConstantValue(feature.settings);
    container.bind<any>('db.connection').toConstantValue(connection);

    const services = await list.services(container);
    const context = (_ as any).merge(...list.context, ...services);

    const mocks = await list.mocks(context);
    const schemas = await list.schema(context);
    const middlewares = await list.middlewares(context);
    
    const directives = await list.directives(context);
    const router = await list.router(feature.settings, context);

    const schemaDirectives = _.assign.apply(_, directives);

    const server = new GraphQLServer({
        mocks,
        schemaDirectives,
        middlewares: middlewares[0], 
        schema: mergeSchemas({ schemas: schemas as any }),
        context: req => _.assign(context, _.merge.apply(_, list.request(req))), 
    } as any);

    server.express.use(cors({ 
        origin: true, 
        credentials: true, 
    }));

    feature.router && server.express.use(...router as any);

    if (module.hot) {
        module.hot.dispose((data) => {
            console.log('DATA: ', data);
            try {
                if (server) {
                    (server.express as any).close();
                }
            } catch (error) {
                console.error(error.stack);
            }
        });
    
        module.hot.accept();
    }

    return new Promise((resolve, reject) => {
        server.start(feature.options, (...data) => resolve(server));
    });
}
