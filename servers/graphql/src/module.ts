import { feature, composed } from '@charlotte/core';
import MailAPIModule from '@charlotte/mail-api/lib/server';
// import FileAPIModule from '@charlotte/file-api/lib/server';
import AuthAPIModule from '@charlotte/auth-api/lib/server';
import { SchemaGraphqlFeature } from '@charlotte/core-graphql';

import CharlotteListModule from '@charlotte/list-module/lib/server';
import CharlotteMailModule from '@charlotte/mail-module/lib/server';

import { SystemModule } from './modules/system';
import { ConfigModule } from './modules/config';

// Application APIs
const API_MODULE = feature(MailAPIModule, AuthAPIModule,);

const APPLICATION_MODULES = feature(CharlotteListModule, CharlotteMailModule);

// Build application
const bundle = feature(SystemModule, ConfigModule, APPLICATION_MODULES, API_MODULE);

if (module.hot) {
    module.hot.status(event => {
        if (event === 'abort' || event === 'fail') {
            // Signal webpack.run.js to do full-reload of the back-end
            process.exit(250);
        }
        if (event === 'idle') {
            process.exit(250);
        }
    });

    module.hot.accept();
}

// Composed module for configuration
export const application = composed(bundle, SchemaGraphqlFeature);
