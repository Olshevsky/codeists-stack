import { injectable, inject, postConstruct } from "inversify";

import { IReportService, ISendingReport } from "../interfaces/IMailService";

@injectable()
export class Log implements IReportService {
    async write(report: ISendingReport) {
        console.log(report);
        return report;
    }
}