import { injectable, inject, postConstruct } from "inversify";
import { IOCType, Status } from '@charlotte/core-charlotte/lib/common';

import { MailReportModelFunc } from "../models/MailReport";
import { IReportService, ISendingReport } from "../interfaces/IMailService";

@injectable()
export class Mongo implements IReportService {
    model: any;
    @inject(IOCType.DBConnection) db;
    
    @postConstruct()
    init() {
        this.model = MailReportModelFunc(this.db);
    }
    
    write(report: ISendingReport) {
        return this.model.create(report);
    }
}