import { interfaces } from 'inversify';
import { feature } from '@charlotte/core';
import { IGraphqlFeature } from '@charlotte/core-graphql';

import { Types } from './constants';
import { container } from './container';

const services = (container: interfaces.Container) => ({
    mail: container.get(Types.MailService),
});

export default feature<IGraphqlFeature>({
    services,
    container,
} as any);
