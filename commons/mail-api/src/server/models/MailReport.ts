import Mongoose, { Schema } from 'mongoose';

import { MailModels } from '../constants';

const { Types } = Schema;
const { ObjectId } = Types;

const MailReportSchema = new Schema({
    name: { type: String },
    description: { type: String },
    picture_url: { type: String },
});

export const MailReportModelFunc = db => 
    db.model(MailModels.MailReport, MailReportSchema);
