import Mongoose, { Schema } from 'mongoose';

import { MailModels } from '../constants';

const { Types } = Schema;
const { ObjectId } = Types;

const TemplateSchema = new Schema({
    name: { type: String },
    description: { type: String },
    picture_url: { type: String },
});

export const TemplateModelFunc = db => 
    db.model(MailModels.Template, TemplateSchema);
