export interface IMailObject {
    to: string;
    _id?: string;
    bcc?: string;
    from: string;
    topic: string;
    variables?: any;
    templateId: string;
    attachments: string;
}

export interface IDateRange {
    to: string | Date;
    from: string | Date;
}

export interface IReportsQuery {
    to: string[];
    from: string[];
    subject: string;
    template: string;
    date: IDateRange;
}

export interface ISendingReport extends IMailObject {
    ok: boolean;
    trace?: any;
}
export interface ISendMailRequest extends IMailObject {}

export interface IMailerService {
    send(request: ISendMailRequest): Promise<ISendingReport>;
}

export interface IReportService {
    write(report: ISendingReport): Promise<ISendingReport>;
}

export interface QueriableService {
    query(q: IReportsQuery): Promise<ISendingReport[]>;
}
