export interface ITemplate {
    text: string;
    html: string;
    engine: string;
    subject: string;
    description: string;
}

export interface ITemplateManagedProvider {
    remove(id: string): Promise<boolean>;
    create(template: ITemplate): Promise<ITemplate>;
    update(id: string, template: ITemplate): Promise<ITemplate>;
}

export interface ITemplateProvider {
    get(id: string): Promise<ITemplate>;
}
