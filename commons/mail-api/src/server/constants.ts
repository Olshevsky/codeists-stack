export enum Types {
    MailService = 'MailService',
    MailTransport = 'MailTransport',
    ReportService = 'ReportService',
    TemplateProvider = 'MongoProvider',
}

export enum MailModels {
    Template = 'Template',
    MailReport = 'MailReport',
}
