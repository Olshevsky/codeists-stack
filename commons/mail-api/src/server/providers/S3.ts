import { S3 as S3Service } from 'aws-sdk';
import { injectable, inject, postConstruct } from "inversify";
import { IOCType, Status } from '@charlotte/core-charlotte/lib/common';

import { TemplateModelFunc } from "../models/Template";
import { ITemplateProvider, ITemplate } from "../interfaces/ITemplateProvider";

@injectable()
export class S3 implements ITemplateProvider {
    service: S3Service;

    public static TEMPLATES_BUKET = 'templates';

    constructor(
        @inject('settings') settings,
    ) {
        this.service = new S3Service({
            endpoint: settings.S3_ENDPOINT,
            credentials: {
                accessKeyId: settings.S3_ACCESS_KEY_ID,
                secretAccessKey: settings.S3_SECRET_ACCESS_KEY,
            },
        });
    }
    
    async get(template: string) {
        return new Promise<ITemplate>((resolve, reject) => {
            this.service.getObject({ Bucket: S3.TEMPLATES_BUKET, Key: template }, (err, data) => {
                if(err) return reject(err);
                else {
                    try {
                        const template = JSON.parse(data.Body as string);
                        return resolve(template);
                    }
                    catch(e) {
                        return reject(e);
                    }
                }
            });
        });
    }
}