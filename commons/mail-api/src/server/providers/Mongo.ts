import { injectable, inject, postConstruct } from "inversify";
import { IOCType, Status } from '@charlotte/core-charlotte/lib/common';

import { TemplateModelFunc } from "../models/Template";
import { ITemplateProvider } from "../interfaces/ITemplateProvider";

@injectable()
export class Mongo implements ITemplateProvider {
    model: any;
    @inject(IOCType.DBConnection) db;
    
    @postConstruct()
    init() {
        this.model = TemplateModelFunc(this.db);
    }
    
    get(template: string) {
        return this.model.findOne({ _id: template });
    }
}