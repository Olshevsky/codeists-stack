import * as _ from 'lodash';
import * as Nodemailer from 'nodemailer';
import * as Email from 'email-templates';
import * as Consolidate from 'consolidate';
import { inject, injectable, optional } from 'inversify';

import { Types } from '../constants';
import { IMailerService, IReportService, ISendMailRequest } from "../interfaces/IMailService";
import { ITemplateManagedProvider, ITemplateProvider } from "../interfaces/ITemplateProvider";

@injectable()
export class MailService implements IMailerService {
    mailer: Email;
    report: IReportService;
    provider: ITemplateProvider;
    
    constructor(
        @inject(Types.ReportService) report,
        @inject(Types.TemplateProvider) provider,
        @inject(Types.MailTransport) @optional() transport,
    ) {
        this.report = report;
        this.provider = provider;
        this.mailer = new Email({
            send: true,
            preview: false,
            transport: transport || Nodemailer.createTransport({
                newline: 'windows',
                streamTransport: true,
            }),
            render: async (templatPath, locals) => {
                const [ templateId, part ] = templatPath.split('/');

                const template = await this.provider.get(templateId);

                if (!template) {
                    throw new Error(`Template ${templateId} not found`);
                }

                const engine = Consolidate[_.get(template, 'engine', 'ejs')];
                const html = await engine.render(_.get(template, part, `Template ${templateId}`), locals);

                return await this.mailer.juiceResources(html);
            },
        });
    }

    async send(request: ISendMailRequest) {
        const result = _.assign({}, request, { ok: false });
        try {
            const mail = await this.mailer.send({
                send: true,
                locals: request.variables,
                template: request.templateId,
                message: _.pick(request, ['from', 'to', 'attachments', 'bcc']),
            });            
            
            _.assign(result, { ok: true, trace: null });
        } catch (e) {
            return _.assign(result, { trace: e, ok: false });
        }

        this.report.write(result);
        return result;
    }
}