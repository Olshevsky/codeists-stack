import { Container } from "inversify";

import { Types } from "./constants";
import { Log } from "./reports/Log";
import { MailService } from "./services/MailService";
import { DigitalOcean } from "./providers/DigitalOcean";
import { ITemplateProvider } from "./interfaces/ITemplateProvider";
import { IMailerService, IReportService } from "./interfaces/IMailService";

export const container = new Container();

container.bind<IReportService>(Types.ReportService).to(Log);
container.bind<IMailerService>(Types.MailService).to(MailService);
container.bind<ITemplateProvider>(Types.TemplateProvider).to(DigitalOcean);
