import * as React from 'react';
import { Auth0 } from '../services';

export class Logout extends React.Component<any, any> {

    componentDidMount() {
        this.props.context.auth.logout()
            .then(() => this.props.history.push('/'))
            .catch((err) => console.error(err));
    }

    render() {
        return <div />;
    }
}
