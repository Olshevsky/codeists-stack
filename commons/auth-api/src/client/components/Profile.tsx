import gql from 'graphql-tag';
import * as React from 'react';
import { Query } from 'react-apollo';

import { authenticated } from '../decorators';
import { PROFILE_QUERY } from '../gql';

@authenticated()
export class Profile extends React.Component<any, any> {
    render() {
        return (
            <div>
                <p>
                    <a href="/logout">Logout!</a>
                </p>
                <Query query={PROFILE_QUERY}>
                    {({ data, loading, error }) => (
                        <div>
                            <pre>{JSON.stringify({ error, data, loading }, null, 2)}</pre>
                        </div>
                    )}
                </Query>
                <hr/>
            </div>
        );
    }
}
