import * as React from 'react';
import { Auth0 } from '../services';

export class Callback extends React.Component<any, any> {

    componentDidMount() {
        const { history, context } = this.props;
        context.auth.handleAuthentication()
            .then(() => history.push(context.auth.redirectPath))
            .catch(err => console.error(err));
    }

    render() {
        return <div />;
    }
}
