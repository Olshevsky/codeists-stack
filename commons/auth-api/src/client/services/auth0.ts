import auth0 from 'auth0-js';
import { resolve } from 'url';
import * as cookie from 'js-cookie';

export class Auth0 {
    auth0;
    userProfile;
    tokenRenewalTimeout;

    constructor(settings) {
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.getProfile = this.getProfile.bind(this);
        this.getAccessToken = this.getAccessToken.bind(this);
        this.isAuthenticated = this.isAuthenticated.bind(this);
        this.handleAuthentication = this.handleAuthentication.bind(this);

        this.auth0 = new auth0.WebAuth({
            domain: settings.AUTH0_DOMAIN,
            responseType: 'token id_token',
            clientID: settings.AUTH0_CLIENT_ID,
            redirectUri: settings.AUTH0_REDIRECT_URI,
            scope: 'openid offline_access app_metadata user_metadata profile',
        });

        this.scheduleRenewal();
    }

    login() {
        this.auth0.authorize();
    }

    setRedirectPath(path: string) {
        localStorage.setItem('__redirect__', path)
    }

    get redirectPath() {
        return localStorage.getItem('__redirect__') || '/'
    }

    get idToken() {
        const idToken = localStorage.getItem('id_token');
        if (!idToken) {
            return '';
        }
        return idToken;
    }

    get accessToken() {
        const accessToken = localStorage.getItem('access_token');
        if (!accessToken) {
            return '';
        }
        return accessToken;
    }

    handleAuthentication() {
        return new Promise((resolve, reject) => {
            this.auth0.parseHash((err, authResult) => {
                if (authResult && authResult.accessToken && authResult.idToken) {
                    this.setSession(authResult);
                    resolve(authResult);
                } else if (err) {
                    reject(err)
                    alert(`Error: ${err.error}. Check the console for further details.`);
                }
            });
        });
    }

    setSession(authResult) {
        // Set the time that the access token will expire at
        let expiresAt = JSON.stringify(
            authResult.expiresIn * 1000 + new Date().getTime()
        );

        localStorage.setItem('access_token', authResult.accessToken);
        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('expires_at', expiresAt);

        // schedule a token renewal
        this.scheduleRenewal();
    }

    getAccessToken() {
        const accessToken = localStorage.getItem('access_token');
        if (!accessToken) {
            throw new Error('No access token found');
        }
        return accessToken;
    }

    getProfile() {
        return new Promise((resolve, reject) => {
            let accessToken = this.getAccessToken();
            this.auth0.client.userInfo(accessToken, (err, profile) => {
                if (err) return reject(err);
                if (profile) {
                    this.userProfile = profile;
                    return resolve(profile)
                }
            });
        });
    }

    logout() {
        return new Promise((resolve) => {
            localStorage.removeItem('access_token');
            localStorage.removeItem('id_token');
            localStorage.removeItem('expires_at');
            localStorage.removeItem('scopes');
            this.userProfile = null;
            clearTimeout(this.tokenRenewalTimeout);

            setTimeout(() => resolve(), 500)
        });
    }

    isAuthenticated() {
        // Check whether the current time is past the
        // access token's expiry time
        let expiresAt = JSON.parse(localStorage.getItem('expires_at'));
        return new Date().getTime() < expiresAt;
    }

    renewToken() {
        this.auth0.checkSession({},
            (err, result) => {
                if (err) {
                    alert(
                        `Could not get a new token (${err.error}: ${err.error_description}).`
                    );
                } else {
                    this.setSession(result);
                    alert(`Successfully renewed auth!`);
                }
            }
        );
    }

    scheduleRenewal() {
        const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
        const delay = expiresAt - Date.now();
        if (delay > 0) {
            this.tokenRenewalTimeout = setTimeout(() => {
                this.renewToken();
            }, delay);
        }
    }
}