import * as React from 'react';

import { Logout } from '../components/Logout';

export default ({ history, context }) => ({
    title: 'Logout',
    component: <Logout history={history} context={context} />,
});
