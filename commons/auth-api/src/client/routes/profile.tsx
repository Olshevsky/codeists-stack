import * as React from 'react';

import { Profile } from '../components/Profile';

export default ({ history, context }) => ({
    title: 'Profile',
    component: <Profile history={history} context={context} />,
});
