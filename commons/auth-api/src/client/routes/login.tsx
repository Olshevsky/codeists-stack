import * as React from 'react';

import { Login } from '../components/Login';

export default ({ history, context }) => ({
    title: 'Login',
    component: <Login history={history} context={context} />,
});
