import * as React from 'react';

import { Callback } from '../components/Callback';

export default ({ history, context }) => ({
    title: 'Callback',
    component: <Callback history={history} context={context} />,
});
