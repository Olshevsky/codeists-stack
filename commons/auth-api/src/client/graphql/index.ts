export const typeDefs = ``;

export const resolvers = (ctx) => ({
    Query: {
        profile: async () => {
            return ctx.auth.getProfile();
        },
    },
});
