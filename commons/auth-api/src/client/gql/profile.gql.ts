import gql from 'graphql-tag';

export default gql`
query profile {
    profile @client
}`;
