import { feature } from "@charlotte/core";
import { setContext } from "apollo-link-context";
import { IReactFeature } from "@charlotte/core-client";

import { Auth0 } from './services';
import { typeDefs, resolvers } from './graphql';

export default feature<IReactFeature>({
    typeDefs, resolvers,
    context: settings => {
        console.log('Resolving context: ', settings);
        return ({ auth: new Auth0(settings) });
    },
    link: ({ auth }) => 
        setContext(() => ({
            headers: {
                authorization: `Bearer ${auth.idToken}`,
            },
        }),
    ),
});