import * as React from 'react';

export const authenticated = () => 
    WrappedComponent => 
        class IsAuthenticated extends React.Component<any, any> {
            componentWillMount() {
                const { history, context } = this.props;
                if (!context.auth.isAuthenticated()) {
                    context.auth.setRedirectPath(history.location.pathname);
                    history.push(context.auth.loginPath || '/login');
                }
            }

            render() {
                return <WrappedComponent {...this.props} />
            }   
        }   
