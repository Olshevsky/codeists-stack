import * as _ from 'lodash';
import { Router } from 'express';
import { feature } from '@charlotte/core';
import * as ExpressJWT from 'express-jwt';
import { IGraphqlFeature } from '@charlotte/core-graphql';
import { makeExecutableSchema } from 'graphql-tools';

import { Auth } from './services';
import { IsAuthenicated, HasRole } from './directives';

const jwks = require('jwks-rsa');

const router = (settings, context) => {
    const router = Router();

    // Routes
    router.use(
        '/graphql',
        ExpressJWT({
            algorithms: ['RS256'],
            credentialsRequired: false,
            issuer: settings.AUTH0_ISSUER,
            audience: settings.AUTH0_AUDIENCE,
            secret: jwks.expressJwtSecret({
                cache: true,
                rateLimit: true,
                jwksRequestsPerMinute: 5,
                jwksUri: `https://${settings.AUTH0_DOMAIN}/.well-known/jwks.json`,
            }),
            getToken: req => {
                if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
                    return req.headers.authorization.split(' ')[1];
                } else if (req.query && req.query.token) {
                    return req.query.token;
                }
                return null;
            },
        }),
        (req: any, res, next) => {
            if (req.user) {
                context.auth.credentials()
                    .then(({ access_token }) => context.auth.profile(req.user.sub, access_token))
                    .then(profile => {
                        req.profile = profile;
                        next()
                    })
                    .catch(err => next(err));
            } else {
                return next()
            }

        },
    );

    // Workaround for expired JWT tokens
    router.use((err, req, res, next) => next());

    return router;
}

const schema = (ctx: any) => makeExecutableSchema({
    typeDefs: `
        directive @isAuthenticated on FIELD_DEFINITION
        directive @hasRole(role: String) on FIELD_DEFINITION

        type UserProfile { 
            nickname: String
            name: String
            picture: String
            updated_at: String
            iss: String
            sub: String
            aud: String
            iat: Int
            exp: Int
        }

        type Query {
            authenticated: Boolean
            profile: UserProfile @isAuthenticated
        }
    `,
    resolvers: {
        Query: {
            profile: (root, args, context) => context.user,
            authenticated: (root, args, context) => !!context.user,
        },
    },
    schemaDirectives: {
        hasRole: HasRole,
        isAuthenticated: IsAuthenicated,
    },
});

export default feature<IGraphqlFeature>({
    router,
    schema,
    context: settings => ({ auth: new Auth(settings) }),
    request: ({ request }) => ({ user: request.user, profile: request.profile }),
} as any);
