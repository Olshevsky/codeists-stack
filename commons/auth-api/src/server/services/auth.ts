import * as request from 'request-promise';

export class Auth {
    settings: any;

    constructor(settings) {
        this.settings = settings;
    }

    credentials() {
        return request({
            json: true,
            method: 'POST',
            url: `https://${this.settings.AUTH0_DOMAIN}/oauth/token`,
            headers: { 'content-type': 'application/json' },
            body: {
                grant_type: 'client_credentials',
                client_id: `${this.settings.AUTH0_CLIENT_ID}`,
                client_secret: `${this.settings.AUTH0_SECRET}`,
                audience: `${this.settings.AUTH0_API_AUDIENCE}`,
            },
        });
    }

    profile(user, token) {
        return request({
            json: true,
            method: 'GET',
            url: `${this.settings.AUTH0_API_AUDIENCE}users/${user}`,
            headers: {
                'content-type': 'application/json',
                authorization: `Bearer ${token}`,
            },
        })
    }
}
