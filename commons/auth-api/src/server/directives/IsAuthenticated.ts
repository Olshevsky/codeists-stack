import { defaultFieldResolver } from 'graphql';
import { SchemaDirectiveVisitor } from "graphql-tools";

export class IsAuthenicated extends SchemaDirectiveVisitor {
  public visitFieldDefinition(field) {
    const { resolve = defaultFieldResolver } = field;

    field.resolve = (...args) => {
      const [, , context] = args;
      if (context.user) {
        return resolve.apply(this, args)
      }

      throw new Error(
        `You are not authorized!`,
      )
    }
  }
}
