import * as _ from 'lodash';
import { defaultFieldResolver } from 'graphql';
import { SchemaDirectiveVisitor } from "graphql-tools";

export class HasRole extends SchemaDirectiveVisitor {
  public visitFieldDefinition(field) {
    const { resolve = defaultFieldResolver } = field
    const { role = '' } = this.args;

    field.resolve = (...args) => {
      const [, , context] = args;
      const profile = _.get(context, 'profile');
      const roles = _.get(profile, 'app_metadata.roles', []);

      if (!role || _.includes(roles, role)) {
        return resolve.apply(this, args)
      }

      throw new Error(
        `You are not authorized. Expected role: ${role}`,
      )
    }
  }
}
