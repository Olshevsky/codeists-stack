import { S3 } from 'aws-sdk';
import { createReadStream } from 'fs';
import { inject, injectable } from "inversify";

import { IStorageAdapter, IAdapterOptions } from "../interfaces";

@injectable()
export class DigitalOcean implements IStorageAdapter {
    service: S3;

    constructor(
        @inject('settings') settings,
    ) {
        this.service = new S3({
            endpoint: settings.DO_S3_ENDPOINT,
            credentials: {
                accessKeyId: settings.DO_ACCESS_KEY_ID,
                secretAccessKey: settings.DO_SECRET_ACCESS_KEY,
            },
        });
    }

    upload(source: string, options: IAdapterOptions) {
        return new Promise<string>((resolve, reject) => {
            this.service.upload({
                Key: options.name,
                ACL: 'public-read',
                Bucket: options.bucket,
                Body: createReadStream(source),
            }, (err, data) => {
                if (err) return reject(err);
                else return resolve(data.Location);
            });
        });
    }

    remove() {
        return Promise.resolve(true);
    }
}
