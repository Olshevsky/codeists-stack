import { S3 as S3Service } from 'aws-sdk';
import { createReadStream } from 'fs';
import { inject, injectable } from "inversify";

import { IStorageAdapter, IAdapterOptions } from "../interfaces";

@injectable()
export class S3 implements IStorageAdapter {
    service: S3Service;
    @inject('settings') settings;

    constructor() {
        this.service = new S3Service({
            credentials: {
                accessKeyId: this.settings.S3_ACCESS_KEY_ID,
                secretAccessKey: this.settings.S3_SECRET_ACCESS_KEY,
            },
        });
    }

    upload(source: string, options: IAdapterOptions) {
        return new Promise<string>((resolve, reject) => {
            this.service.upload({
                Key: options.name,
                ACL: 'public-read',
                Bucket: options.bucket,
                Body: createReadStream(source),
            }, (err, data) => {
                if(err) return reject(err);
                else return resolve(data.Location);
            });
        });
    }

    remove() {
        return Promise.resolve(true);
    }
}
