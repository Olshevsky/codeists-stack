import { Container } from "inversify";

import { Types } from "./constants";
import { S3 } from "./adapters/S3";
import { FileService } from "./services/FileService";
import { IStorageAdapter, IFileService } from "./interfaces";

export const container = new Container();

container.bind<IFileService>(Types.FileService).to(FileService);
container.bind<IStorageAdapter>(Types.FileStorageAdapter).to(S3);