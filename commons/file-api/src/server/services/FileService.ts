import { inject, injectable } from "inversify";

import { Types } from '../constants';
import { IFileService, IStorageAdapter, IAdapterOptions } from "../interfaces";

@injectable()
export class FileService implements IFileService {
    @inject(Types.FileStorageAdapter) adapter: IStorageAdapter;

    upload(source: string, options: IAdapterOptions) {
        return this.adapter.upload(source, options);
    }

    remove(id) {
        return Promise.resolve(true); 
    } 
}
