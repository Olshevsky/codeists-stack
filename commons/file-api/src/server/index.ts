import { interfaces } from 'inversify';
import { feature } from '@charlotte/core';
import { IGraphqlFeature } from '@charlotte/core-graphql';

import { router } from './router';
import { Types } from './constants';
import { container } from './container';

const services = (container: interfaces.Container) => ({
    file: container.get(Types.FileService),
});

export default feature<IGraphqlFeature>({
    router,
    services,
    container,
} as any);
