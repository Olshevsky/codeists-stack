export interface IFileService {
    remove(id: string): Promise<boolean>;
    upload(source: string, options: any): Promise<string>
}
