export interface IAdapterOptions {
    name: string;
    meta?: string;
    bucket: string;
}

export interface IStorageAdapter {
    remove(id: string): Promise<boolean>;
    upload(source: string, options: IAdapterOptions): Promise<string>;
}
