import uuid from 'uuid/v4';
import * as multer from 'multer';
import { Router } from 'express';

export const router = ((settings, context) => {
    const fileApi = Router();
    const upload = multer({ dest: settings.TMP_DIR });

    fileApi.post('/upload/:bucket', upload.single('file'), (req: any, res) => {
        const file = req.file;
        const name = file.originalname || `${uuid()}-${file.originalname}`;

        context.file.upload(file.path || file.buffer, { bucket: req.params.bucket, name })
            .then(url => res.send(url))
            .catch(err => res.status(500).send(err))
    });

    return fileApi;
}) as any;
