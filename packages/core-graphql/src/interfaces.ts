import { GraphQLSchema } from 'graphql';
import { RequestHandler } from 'express';
import { IMiddleware } from 'graphql-middleware';
import { Options as ServerOptions } from 'graphql-yoga';
import { schema, IPart, IFeature } from '@charlotte/core';

export interface IGraphqlFeature extends IFeature {
    mocks?: any;
    context?: any;
    request?: any;
    sources?: any;
    services?: any;
    schemaDirectives?: any;
    options?: ServerOptions;
    middlewares?: IMiddleware[];
    schema?: (ctx: any) => GraphQLSchema[];
    router?: (settings: any ,ctx: any) => RequestHandler;
}
