import { IEntityIcon } from './IEntityIcon';

export interface ISection {
    name: string;
    active: boolean;
    icon: IEntityIcon;
    description: string;
}