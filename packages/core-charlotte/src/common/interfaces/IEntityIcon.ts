export interface IEntityIcon {
    url?: string;
    class?: string;
}