export interface IMessage {
    text: string[];
    read: string[];
    thread: string;
    status: string;
    isRead: boolean;
    creator: string[];
    attachments: string[];
}

export interface IThreadView {
    favorite: string[];
    archived: string[];
}

export interface IThread {
    status: string;
    unread: number;
    users: string[];
    product: string;
    view: IThreadView;
    isFavorite: boolean;
    isArchived: boolean;
}
