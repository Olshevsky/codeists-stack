export interface IFeedbackAnswer {
    answer: string;
    question: string;
}

export interface IOrderFeedback {
    rate: number;
    notes: string;
    answers: IFeedbackAnswer[];
}

export interface IOrder {
    qty: string;
    user: string;
    price: number;
    notes: string;
    status: string;
    product: string;
    merchant: string;
    feedback: IOrderFeedback;
}