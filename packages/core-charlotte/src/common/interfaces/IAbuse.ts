export enum AbuseType {
    User = 'abuse.user',
    Product = 'abuse.product',
}

export interface IAbuse {
    user: string;
    reason: string;
    comment: string;
    type: AbuseType;
}
