export interface IProductNotes {
    delivery: string;
    consistency: string;
}

export interface IProductAttribute {
    value: string;
    attribute: string;
}

export interface IProduct {
    name: string;
    price: number;
    group: string;
    brand: string;
    section: string;
    merchant: string;
    pictures: string[];
    available: boolean;
    notes: IProductNotes;
    attributes: IProductAttribute[];
}
