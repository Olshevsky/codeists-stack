import { IEntityIcon } from './IEntityIcon';

export interface ISectionGroup {
    name: string;
    active: boolean;
    section: string;
    icon: IEntityIcon;
    description: string;
}

export interface IAttributeFilter {
    name: string;
    type: string;
}

export interface IAttribute {
    name: string;
    type: string;
    values: any[];
    group: string;
    icon: IEntityIcon;
    filter: IAttributeFilter;
}
