export enum Status {
    Active = 'status.active',
    Removed = 'status.removed',
    Blocked = 'status.blocked',
    Archived = 'status.archived',

    Done = 'status.done',
    Pending = 'status.pending',
    Declined = 'status.declined',
    Approved = 'status.approved',
    Requested = 'status.requested',
}

export enum IOCType {
    DBConnection = 'db.connection'
}
