export interface CRUDService<T> {
    read(id: any): Promise<any>;
    remove(id): Promise<boolean>;
    create(data: T): Promise<any>;
    list(query: any): Promise<any[]>;
    update(id, data: T): Promise<any>;
}