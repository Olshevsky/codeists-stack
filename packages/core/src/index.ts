import * as _ from 'lodash';
import { Container, interfaces } from 'inversify';
 
export const combine = (features, extractor) => _.without(_.union(..._.map(features, res => _.castArray(extractor(res)))), undefined);

export function mixin(target = {}, ...sources: any[]): any {
    const result = _.merge({}, target);
    sources.forEach(source => {
        Object.getOwnPropertyNames(source).forEach(name => {
            if (Array.isArray(result[name]))
                result[name] = _.union(result[name], source[name]);
            else if (_.isObject(result[name]))
                result[name] = _.merge(result[name], source[name]);
            else
                result[name] = source[name];
        });
    });

    return result;
}

export interface IPart {
    definition?: any;
    composer?: (arr: any[]) => any | any[];
}

export interface IFeature {
    context?: any;
    settings?: any;
    container?: interfaces.Container;
}

export function feature<T>(def: T, ...nested: T[]): T {
    const features = nested.concat([def]);
    const keys = _.union(_.flatten(features.map(feature => _.keys(feature))));
    const combined = keys.reduce((acc, key) => _.assign({}, acc, { [key]: combine(features, feature => feature[key]) }), {});
    return combined;
}

export interface ISchema {
    [propName: string]: IPart;
}

export interface ICompletedSchema extends ISchema {
    context: IPart;
    settings: IPart;
    container: IPart;
}

export function composed<T>(feature: T, schema: ISchema): T {
    const fields = _.pick(feature, _.keys(schema));
    return _.reduce(fields, (acc, part, key) => _.merge(acc, { [key]: _.get(schema[key], 'composer', arg => arg)(part, fields) }), {});
}

export const ContainerPart: IPart = {
    composer: (containers: interfaces.Container[]) => !_.isEmpty(containers)
        ? _.reduce(containers, (acc, container) => Container.merge(acc, container || new Container), new Container())
        : new Container(),
};

export function schema(def: ISchema): ICompletedSchema {
    return _.merge(def, {
        context: {},
        container: ContainerPart,
        settings: {
            composer: (settings: any = []) => _.merge(...settings),
        },
    }) as ICompletedSchema;
}