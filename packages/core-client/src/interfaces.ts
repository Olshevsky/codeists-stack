import { ApolloLink } from "apollo-link";
import { IFeature } from "@charlotte/core";

export interface IRoute {
    path: string;
    index?: number;
    load?: () => Promise<any>;
    action?: (...args: any[]) => any;
}

export interface IReactFeature extends IFeature {
    defaults?: any;
    typeDefs?: any;
    reducers?: any;
    services?: any;
    resolvers?: any;
    routes?: IRoute[];
    link?: (context: any) => ApolloLink | Promise<ApolloLink>;
}

export interface IRenderPropsComponent {
    render(...data: any[]): any;
}
