/* tslint:disable */
import * as _ from 'lodash';
import { ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { ApolloClient } from 'apollo-client';
import { WebSocketLink } from 'apollo-link-ws';
import { withClientState } from 'apollo-link-state';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';

import { IReactFeature } from './interfaces';
import * as APIModule from './api-module';

export async function createApollo(pkg: IReactFeature, context: any) {
    const endpoint = pkg.settings.APOLLO_URI || `http://${location.host}/graphql`;

    const http = new HttpLink({ uri: endpoint });
    const list = await Promise.all(_.map(pkg.link, link => link(context))) as any;
    const resolvers = await Promise.all(_.map(pkg.resolvers, resolvers => resolvers(context)));

    const composed = ApolloLink.from(list).concat(http);
    const state = withClientState({
        resolvers: _.merge(APIModule, ...resolvers),
        typeDefs: APIModule.typeDefs.concat(pkg.typeDefs),
        defaults: _.merge(APIModule.defaults, pkg.defaults),
    });

    const link = state.concat(composed);

    return new ApolloClient({
        link,
        cache: new InMemoryCache(),
    });
}
