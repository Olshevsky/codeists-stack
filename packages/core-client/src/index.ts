import * as _ from "lodash";
import React, { createContext } from "react";
import { ApolloLink } from "apollo-link";
import { schema, IPart, ICompletedSchema } from '@charlotte/core';

import { createStore } from './createStore';
import { createApollo } from './createApollo';
import { IReactFeature, IRoute } from './interfaces';

export enum ROUTE_INDEX {
    TOP_ROUTE = Number.MIN_SAFE_INTEGER,
    SYSTEM_ROUTE = Number.MAX_SAFE_INTEGER,
};

const ReducersPart: IPart = {
    composer: (reducers: any[]) => _.merge(...reducers),
};

const ResolversPart: IPart = {
    composer: (resolvers: any[]) => _.merge(...resolvers),
};

const TypeDefsPart: IPart = {
    composer: (typeDefs: any[]) => typeDefs.join('\n'),
};

const DefaultsPart: IPart = {
    composer: (defaults: any[]) => _.merge(...defaults),
};

// const ApolloPart: IPart = {
//     composer: (links: any[]) => ApolloLink.from(links),
// };

const SchemaReactFeature = schema({
    link: {},
    routes: {},
    context: {},
    services: {},
    resolvers: {},
    // link: ApolloPart,
    reducers: ReducersPart,
    typeDefs: TypeDefsPart,
    defaults: DefaultsPart,
});

const ReactContext = createContext(null);
export { ReactContext, createStore, IReactFeature, IPart, ReducersPart, SchemaReactFeature, createApollo };

