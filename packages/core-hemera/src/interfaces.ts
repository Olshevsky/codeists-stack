import { ServerPattern, Response } from 'nats-hemera';
import { schema, IPart, IFeature } from '@charlotte/core';

export interface IAbstractPattern extends ServerPattern {
    cmd: string;
    topic: string;
    [propName: string]: any;
}

export type IHemeraReply = Response & {
    (err: any, data: any): any
};

export interface IHemeraMiddleware {
    (req: any, reply: IHemeraReply, next?: () => {}): Promise<void>;
}

export interface IHemeraAction {
    middleware: any[],
    pattern: IAbstractPattern;
    action: (req: any, res: IHemeraReply) => any,
}

export interface IHemeraFeature extends IFeature {
    config?: any;
    actions?: IHemeraAction[];
}
